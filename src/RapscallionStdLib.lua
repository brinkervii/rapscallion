local unpack = unpack or table.unpack

---@param machine LISPMachine
---@param context LISPContext
local function stdLib(machine, context)
	local exec = machine.exec

	context.variables.t = function(node)
		machine.printer(node)
	end

	function context.fn:print(...)
		return machine.printer(...)
	end

	function context.special_fn:setq(argumentNodes)
		if argumentNodes[1] then
			argumentNodes[1].data.quoted = true
		end
	end

	function context.fn:setq(variableName, value)
		self.variables[variableName] = value
		return value
	end

	context.special_fn.defvar = context.special_fn.setq
	context.fn.defvar = context.fn.setq
	context.special_fn.defparameter = context.special_fn.defparameter
	context.fn.defparameter = context.fn.setq

	function context.fn:format(output, formatString, ...)
		local args = { ... }

		if #args <= 0 then
			output(formatString)
		else
			output(string.format(formatString, ...))
		end
	end

	function context.fn:read()
		return "updog"
	end

	function context.fn:endless(startingPoint)
		local current = startingPoint

		return function()
			local v = current
			current = current + 1
			return v
		end
	end

	function context.fn:take(amount, factory)
		local list = {}
		for i = 1, amount do
			table.insert(list, factory())
		end

		return list
	end

	function context.special_fn:quote(nodes)
		for _, node in pairs(nodes) do
			node.data.quoted = true
		end
	end

	function context.fn:quote(...)
		return unpack({ ... })
	end

	function context.fn:eq(a, b)
		return a == b
	end

	context.special_fn["if"] = function(_, nodes)
		for i, v in pairs(nodes) do
			v.data.noExec = i ~= 1
		end
	end

	context.fn["if"] = function(_, cond, consequent, otherwise)
		if cond then
			consequent.data.noExec = false
			return exec(consequent)
		else
			if otherwise then
				otherwise.data.noExec = false
				return exec(otherwise)
			end
		end
	end
end

return stdLib
