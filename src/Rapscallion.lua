local unpack = unpack or table.unpack

local TOKEN_ID = {
	LIST_START = "LIST_START",
	LIST_END = "LIST_END",
	ESCAPE = "ESCAPE",
	QUOTE = "QUOTE",
	STRING_OPEN = "STRING_OPEN",
	STRING_CLOSE = "STRING_CLOSE",
	STRING = "STRING",
	ATOM = "ATOM",
	COMMENT_OPEN = "COMMENT_OPEN",
	COMMENT_CLOSE = "COMMENT_CLOSE",
	COMMENT = "COMMENT",
	MULTILINE_COMMENT_START = "MULTILINE_COMMENT_START",
	MULTILINE_COMMENT_END = "MULTILINE_COMMENT_END",
	MULTILINE_COMMENT = "MULTILINE_COMMENT",
	KEYWORD_START = "KEYWORD_START",
	KEYWORD = "KEYWORD",
	NIL = "NIL"
}

local TOKENS = {
	[TOKEN_ID.LIST_START] = "(",
	[TOKEN_ID.LIST_END] = ")",
	[TOKEN_ID.ESCAPE] = "\\",
	[TOKEN_ID.QUOTE] = "'",
	[TOKEN_ID.STRING_OPEN] = [["]],
	[TOKEN_ID.STRING_CLOSE] = [["]],
	[TOKEN_ID.STRING] = 1,
	[TOKEN_ID.ATOM] = 2,
	[TOKEN_ID.COMMENT_OPEN] = ";",
	[TOKEN_ID.COMMENT_CLOSE] = "\n",
	[TOKEN_ID.COMMENT] = 3,
	[TOKEN_ID.MULTILINE_COMMENT_START] = "#||",
	[TOKEN_ID.MULTILINE_COMMENT_END] = "||#",
	[TOKEN_ID.MULTILINE_COMMENT] = 4,
	[TOKEN_ID.KEYWORD_START] = ":",
	[TOKEN_ID.KEYWORD] = 5,
	[TOKEN_ID.NIL] = "nil"
}

local TOKENS_INVERSE = {}
do
	for k, v in pairs(TOKENS) do
		TOKENS_INVERSE[v] = k
	end
end

local AST_NODE_ID = {
	LISP_PROGRAM = "LISP_PROGRAM",
	LISP_LIST = "LISP_LIST",
	LISP_ATOM = "LISP_ATOM",
	LISP_KEYWORD = "LISP_KEYWORD",

	LUA_STRING = "LUA_STRING",
	LUA_LIST = "LUA_LIST",
	LUA_NUMBER = "LUA_NUMBER",
	LUA_FUNCTION = "LUA_FUNCTION",
	LUA_ANY = "LUA_ANY"
}

local function string_trim(s)
	return (s:gsub("^%s*(.-)%s*$", "%1"))
end

local function string_endswith(s, endsWith)
	return not not s:match(endsWith .. "$")
end

local STEPPERS = {}

---global
---@param tokenizer Tokenizer
---@param context table
---@param character string
function STEPPERS.global(tokenizer, context, character)
	local append, nextToken = tokenizer.append, tokenizer.nextToken

	if tokenizer.currentTokenString == TOKENS[TOKEN_ID.MULTILINE_COMMENT_START] then
		tokenizer.useStepper(STEPPERS.multilineComment)
		tokenizer.performStep(character)

	elseif tokenizer.currentTokenString == TOKENS[TOKEN_ID.NIL] then
		nextToken(TOKENS[TOKEN_ID.NIL])
		tokenizer.performStep(character)

	elseif character == TOKENS[TOKEN_ID.LIST_START] then
		append(character)
		nextToken(TOKENS[TOKEN_ID.LIST_START])

	elseif character == TOKENS[TOKEN_ID.LIST_END] then
		nextToken(TOKENS[TOKEN_ID.ATOM])
		append(character)
		nextToken(TOKENS[TOKEN_ID.LIST_END])

	elseif character == TOKENS[TOKEN_ID.ESCAPE] then
		append(character)
		nextToken(TOKENS[TOKEN_ID.ESCAPE])

	elseif character == TOKENS[TOKEN_ID.QUOTE] then
		append(character)
		nextToken(TOKENS[TOKEN_ID.QUOTE])

	elseif character == TOKENS[TOKEN_ID.STRING_OPEN] then
		append(character)
		tokenizer.useStepper(STEPPERS.string)

	elseif character == TOKENS[TOKEN_ID.COMMENT_OPEN] then
		tokenizer.useStepper(STEPPERS.comment)

	elseif character == TOKENS[TOKEN_ID.KEYWORD_START] then
		tokenizer.useStepper(STEPPERS.keyword)

	elseif character:match("%s") then
		nextToken(TOKENS[TOKEN_ID.ATOM])

	else
		append(character)
	end
end

---string
---@param tokenizer Tokenizer
---@param context table
---@param character string
function STEPPERS.string(tokenizer, context, character)
	if character == TOKENS[TOKEN_ID.STRING_CLOSE] then
		tokenizer.append(character)
		tokenizer.nextToken(TOKENS[TOKEN_ID.STRING])
		tokenizer.useStepper(tokenizer.previousStepper)
	elseif character == "\n" then
		error("Unexpected end of line, expected closing string character")
	else
		tokenizer.append(character)
	end
end

---comment
---@param tokenizer Tokenizer
---@param context table
---@param character string
function STEPPERS.comment(tokenizer, context, character)
	if character == TOKENS[TOKEN_ID.COMMENT_OPEN] then
		-- do nothing
	elseif character == TOKENS[TOKEN_ID.COMMENT_CLOSE] then
		tokenizer.nextToken(TOKENS[TOKEN_ID.COMMENT])
		tokenizer.useStepper(tokenizer.previousStepper)
	else
		tokenizer.append(character)
	end
end

---multilineComment
---@param tokenizer Tokenizer
---@param context table
---@param character string
function STEPPERS.multilineComment(tokenizer, context, character)
	if string_endswith(tokenizer.currentTokenString, TOKENS[TOKEN_ID.MULTILINE_COMMENT_END]) then
		tokenizer.nextToken(TOKENS[TOKEN_ID.MULTILINE_COMMENT])
		tokenizer.useStepper(tokenizer.previousStepper)
	else
		tokenizer.append(character)
	end
end

---keyword
---@param tokenizer Tokenizer
---@param context table
---@param character number
function STEPPERS.keyword(tokenizer, context, character)
	if character:match("[%s%W]") then
		tokenizer.nextToken(TOKENS[TOKEN_ID.KEYWORD])
		tokenizer.useStepper(tokenizer.previousStepper)
		tokenizer.performStep(character)
	else
		tokenizer.append(character)
	end
end

local function tokenize(source)
	local tokens = {}

	local currentToken = {}

	---@class Tokenizer
	local tokenizer = {
		stepperContext = {},
		stepper = STEPPERS.global,
		previousStepper = nil,
		currentTokenString = "",
		position = {
			line = 1,
			column = 0
		}
	}

	function tokenizer.nextToken(tokenType)
		local tokenString = table.concat(currentToken, "")
		local trimmedTokenString = string_trim(tokenString)

		if trimmedTokenString ~= "" then
			---@class TokenPosition
			---@field line number
			---@field column number
			local tokenPosition = {
				line = tokenizer.position.line,
				column = tokenizer.position.column
			}

			---@class Token
			local token = {
				value = tokenString,
				type = TOKENS_INVERSE[tokenType],
				position = tokenPosition
			}

			table.insert(tokens, token)
		end

		currentToken = {}
		tokenizer.currentTokenString = ""
	end

	function tokenizer.append(character)
		table.insert(currentToken, character)
		tokenizer.currentTokenString = table.concat(currentToken, "")
	end

	function tokenizer.useStepper(stepper)
		tokenizer.stepperContext = {}
		tokenizer.previousStepper = tokenizer.stepper
		tokenizer.stepper = stepper
	end

	function tokenizer.performStep(character)
		tokenizer.stepper(tokenizer, tokenizer.stepperContext, character)

		if character == "\n" then
			tokenizer.position.column = 0
			tokenizer.position.line = tokenizer.position.line + 1
		else
			tokenizer.position.column = tokenizer.position.column + 1
		end
	end

	for i = 1, #source do
		tokenizer.performStep(source:sub(i, i))
	end

	tokenizer.nextToken(TOKENS[TOKEN_ID.ATOM])

	return tokens
end

---map
---@param haystack table
---@param mapper function
local function map(haystack, mapper)
	local result = {}
	for k, v in pairs(haystack) do
		result[k] = mapper(v)
	end

	return result
end

local function walkAST(ast, visitor)
	local stack = { ast }

	while #stack > 0 do
		local top = table.remove(stack, #stack)

		visitor(top)

		if top.children then
			for _, v in pairs(top.children) do
				table.insert(stack, v)
			end
		end
	end
end

local function makeAST(tokens)
	---@class ASTNode
	local root = {
		originalValue = { "()" },
		originalType = { AST_NODE_ID.LISP_PROGRAM },
		value = "",
		type = AST_NODE_ID.LISP_PROGRAM,
		children = {},
		parent = nil,
		---@type ASTNodeData
		data = nil
	}

	---@class ASTNodeData
	local rootData = {
		quoted = false,
		noExec = false,
		---@type TokenPosition
		position = {
			line = 0,
			column = 0
		}
	}

	root.data = rootData

	---@type ASTNode
	local currentNode = root
	---@type Token
	local previousToken;

	---makeNode
	---@param type string
	---@param value string
	---@param position TokenPosition
	---@param parent ASTNode
	local function makeNode(type, value, position, parent)
		---@type ASTNodeData
		local data = {
			position = position
		}

		---@type ASTNode
		local node = {
			originalValue = { value },
			value = value,
			originalType = { type },
			type = type,
			children = {},
			parent = parent,
			data = data
		}

		if previousToken then
			if previousToken.type == TOKEN_ID.QUOTE then
				node.data.quoted = true
			end
		end

		setmetatable(node, {
			__newindex = function(t, k, v)
				if k == "type" then
					table.insert(t.originalType, t.type)

				elseif k == "value" then
					table.insert(t.originalValue, t.value)

				end

				rawset(t, k, v)
			end
		})

		return node
	end

	for _, token in pairs(tokens) do
		if token.type == TOKEN_ID.LIST_START then
			currentNode = makeNode(AST_NODE_ID.LISP_LIST, token.value, token.position, currentNode)
			table.insert(currentNode.parent.children, currentNode)

		elseif token.type == TOKEN_ID.ATOM then
			local atom = makeNode(AST_NODE_ID.LISP_ATOM, token.value, token.position, nil)
			table.insert(currentNode.children, atom)

		elseif token.type == TOKEN_ID.KEYWORD then
			local keyword = makeNode(AST_NODE_ID.LISP_KEYWORD, token.value, token.position, currentNode)
			table.insert(currentNode.children, keyword)

		elseif token.type == TOKEN_ID.STRING then
			local str = token.value:match(TOKENS[TOKEN_ID.STRING_OPEN] .. "(.+)" .. TOKENS[TOKEN_ID.STRING_CLOSE])
			local stringNode = makeNode(AST_NODE_ID.LUA_STRING, str, token.position, currentNode)
			table.insert(currentNode.children, stringNode)

		elseif token.type == TOKEN_ID.LIST_END then
			currentNode = currentNode.parent
		end

		previousToken = token
	end

	walkAST(root, function(node)
		node.parent = nil
	end)

	return root
end

local function machine()
	---@class LISPContext
	local context = {
		special_fn = {},
		fn = {},
		variables = {}
	}

	---@class LISPMachine
	---@field eval function
	---@field exec function
	---@field printer function
	local lispMachine = {    }

	function lispMachine.printer(...)
		local t = {}
		for _, v in pairs({ ... }) do
			if type(v) == "table" then
				local t2 = {}
				for _, b in pairs(v) do
					table.insert(t2, tostring(b))
				end

				table.insert(t, string.format("{ %s }", table.concat(t2, ", ")))
			else
				table.insert(t, tostring(v))
			end
		end

		print(table.concat(t, " "))
	end

	function resolveFunction(key)
		local f = context.fn[key]
		if type(f) ~= "function" then
			error("Could not resolve function " .. key)
		end

		return f
	end

	---createScope
	---@param superior LISPContext
	function createScope(superior)
		--TODO: Create scopes
		return superior
	end

	function resolveVariable(key)
		if not key then
			error("Unable to resolve nil key")
		end

		local value = context.variables[key]
		if value then
			return value
		end

		error("Unable to resolve variable " .. key)
	end

	local exec;

	---luafyNode
	---@param node ASTNode
	---@param parent ASTNode
	local function luafyNode(node, parent)
		if node.data.noExec then
			return node
		end

		if type(node) ~= "table" then
			error("Node must be a table")
		end

		if node.type == AST_NODE_ID.LISP_LIST and node.data.quoted then
			local list = {}
			for k, v in pairs(node.children) do
				list[k] = luafyNode(v, node)
			end

			-- Convert all atoms to strings since the list is quoted
			for _, v in pairs(list) do
				if v.type == AST_NODE_ID.LISP_ATOM then
					v.type = AST_NODE_ID.LUA_STRING
				end
			end

			node.type = AST_NODE_ID.LUA_LIST
			node.value = list

		elseif node.type == AST_NODE_ID.LISP_LIST and not node.data.quoted then
			---@type ASTNode
			local callNode = node.children[1]

			node.type = AST_NODE_ID.LUA_FUNCTION
			node.value = function()
				local args = {}

				local specialFn = context.special_fn[callNode.value]
				if specialFn then
					local specialArgs = {}
					if #node.children > 1 then
						for i = 2, #node.children do
							table.insert(specialArgs, node.children[i])
						end
					end
					specialFn(context, specialArgs)
				end

				local luafiedChildren = map(node.children, function(c)
					local ok, result = pcall(function()
						return luafyNode(c)
					end)

					if not ok then
						error(result)
					end

					return result
				end)

				if #luafiedChildren > 1 then
					for i = 2, #luafiedChildren do
						table.insert(args, exec(luafiedChildren[i]))
					end
				end

				local fnName = luafiedChildren[1].value
				local fn;
				if type(fnName) == "function" then
					fn = fnName
				else
					fn = resolveFunction(fnName)
				end

				local value = fn(createScope(context), unpack(args))
				if type(value) == "function" then
					node.type = AST_NODE_ID.LUA_FUNCTION
					node.value = value
				end

				return value
			end

		elseif node.type == AST_NODE_ID.LISP_ATOM then
			if node.value:match("%d%.%d") then
				-- Double
				node.type = AST_NODE_ID.LUA_NUMBER
				node.value = tonumber(node.value)

			elseif node.value:match("%d") then
				-- Integer
				node.type = AST_NODE_ID.LUA_NUMBER
				node.value = tonumber(node.value)

			elseif node.data.quoted then
				node.type = AST_NODE_ID.LUA_STRING
				node.value = tostring(node.value)

			end
		end

		return node
	end

	---apply
	---@param astNode ASTNode
	exec = function(astNode)
		if astNode.data.noExec then
			return astNode
		end

		local errorString = "There was an error while generating the error"

		local function doExec()
			if astNode.type == AST_NODE_ID.LISP_PROGRAM then
				local list = {}
				for _, child in pairs(astNode.children) do
					table.insert(list, exec(child))
				end

				local n = #list
				if n == 0 then
					return nil
				elseif n == 1 then
					return list[1]
				else
					return list
				end

				error("Murphy's law struck again")
			end

			local node = luafyNode(astNode)

			if node.type == AST_NODE_ID.LUA_FUNCTION then
				return node.value()

			elseif node.type == AST_NODE_ID.LUA_LIST then
				local list = {}
				for _, v in pairs(node.children) do
					table.insert(list, exec(v))
				end

				return list

			elseif node.type == AST_NODE_ID.LUA_STRING then
				return node.value

			elseif node.type == AST_NODE_ID.LUA_NUMBER then
				return node.value

			elseif node.type == AST_NODE_ID.LISP_ATOM then
				if node.data.quoted then
					return node.value
				else
					return resolveVariable(node.value)
				end

			elseif node.type == AST_NODE_ID.LISP_KEYWORD then
				return node.value

			end

			error("Unable to exec node " .. node.value)
		end

		local function onExecError(...)
			errorString = string.format(
				"Error on line %d, column %d: near node type %s with value '%s'\n\n%s",
				astNode.data.position.line,
				astNode.data.position.column,
				astNode.originalType[1],
				astNode.originalValue[1],
				debug.traceback(...)
			)                   :gsub("\r", "")
		end

		local ok, res = xpcall(doExec, onExecError)
		if not ok then
			error(errorString)
			return nil
		end

		return res
	end
	lispMachine.exec = exec

	function lispMachine.eval(source)
		local tokens = tokenize(source)
		local ast = makeAST(tokens)

		return exec(ast)
	end

	---@param extender function
	function lispMachine.extend(extender)
		extender(lispMachine, context)
	end

	lispMachine.extend(require("RapscallionStdLib"))

	return lispMachine
end

local module = {}
module.machine = machine

return module
