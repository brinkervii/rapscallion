local Rapscallion = require("Rapscallion")

local file = io.open("./dev-test.lisp", "r")
local source = file:read("*a")
file:close()

local machine = Rapscallion.machine()
machine.eval(source)
