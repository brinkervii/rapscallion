local rs = require("Rapscallion")
local machine = rs.machine()

print([[
 ____                           _ _ _
|  _ \ __ _ _ __  ___  ___ __ _| | (_) ___  _ __
| |_) / _` | '_ \/ __|/ __/ _` | | | |/ _ \| '_ \
|  _ < (_| | |_) \__ \ (_| (_| | | | | (_) | | | |
|_| \_\__,_| .__/|___/\___\__,_|_|_|_|\___/|_| |_|
           |_|
]])

local running = true
while running do
	xpcall(function()
		io.write(">> ")
		io.flush()

		local input = io.read()
		if input == "(quit)" then
			running = false
			return
		end

		machine.printer(machine.eval(input))
	end, function(...)
		print(debug.traceback(...))
	end)
end

print("Ciao")
