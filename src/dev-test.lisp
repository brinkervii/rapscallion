(if (eq 'dog 'cat)
	(print "Dogs!")
	(print "Rats!"))

(print ((endless 1)))
(print (quote (a b c d)))

(setq *king-nothing* nil)
(take 10 (endless 1))
(format t "Hello world ~%")
(print "Testing nested function calls :D" (read))

;;;; Program descriptor
;;; Regular comment
;; Indented
; Basic b

#||
Multiline comment
:D
||#

'(a b c d)
'(4 5 3 1 2 23)
(setq *some-numbers* '(4 5 3 1 2 23))

(setq *print-case* :capitalize)


(print "What's your name?")
(defvar *name* (read))

(defun hello-you (name)
	(format t "Hello ~a! ~%" name))

(hello-you *name*)

(+ 5 4) ; This is a basic form :o

(defvar *num* 6)
(setf *num* 1000000)

(format t "Number with commands ~:d ~%" *num*)
(print (/ 5 4))

(print (eq 'dog 'dog))
(print (eq 'dog 'banana))

(defparameter *the-name* 'BrinkerVII)
(format t "The name = ~d ~%" (eq *the-name* *the-name*))

(format t "Equalp ~d ~%" (equalp "banana" "banana"))

(defvar *age* 18)

(if (>= *age* 18)
	(print "You can vote")
	(print "You cannot vote"))
	
(setf *num* 2)

(if (= *num* 2)
	(progn
		(print "Operation 1")
		(print "Operation 2")))
		
(defun get-school (age)
	(case age
		(5 (print "Kindergarten"))
		(6 (print "First grade"))
		(otherwise (print "College"))))
		
(get-school 5)
(get-school 6)
(get-school 18)

(when (= *age* 18)
	(print "Go to college you noob")
	(print "xd"))
	
(unless (= *age* 18)
	print("Something random"))
	
(defvar *college-ready* nil)

(cond
	((>= *age* 18)
		(setf *college-ready* 'yes)
		(print "Ready for college!"))
	
	((< *age* 18)
		(setf *college-ready* 'no)
		(print "Not ready for college"))
	(t (print "Dunno")))
	
(print *college-ready*)

(defun かわいい ()
	(print "This is so cute"))
	
(かわいい)

(defun ᚫ ()
	(print "F"))
	
(ᚫ)

(defvar *numbers* '())
(loop for x from 1 to 10 do
	(progn
		(print x)))


(setq x 1)

(loop
	(format t "Crummy loop ~d~%" x)
	(setq x (+ x 1))
	(push x *numbers*)
	(when (> x 10) (return x)))

(format t "~a~%" *numbers*)

(loop for person in '(Ersko Austin) do
	(format t "This person is ~s ~%" person))
	
(cons 'superman 'batman)

(defvar *superman* (list :name "Superman" :secret-id "Clark Kent"))
(defvar *batman* (list :name "Batman" :secret-id "Bruce wayne"))

(defvar *heroes* (list *superman* *batman*))
(dolist (hero *heroes*)
	(format t "~{~a : ~a ~}~%" hero))
	
(defparameter *assoc-heroes*
		`((Superman (Clark Kent))
		(Flash (Barry Allen))
		(Batman (Bruce Wayne))))
		
(format t "Superman Data ~a~%" (assoc 'superman *assoc-heroes*))

(defun hello ()
	(print "Hello World")
	(terpri))
	
(hello)

(defun sum (&rest numbers)
	(let ((total 0))
		(dolist (num numbers)
			(setf total (+ total num)))
		(format t "The sum is ~d~%" total)))
		
(sum 1 2 3 4 5 6 7 8 9)

(defun 🌴 ()
	(print "Palm tree function"))
	
(🌴)

